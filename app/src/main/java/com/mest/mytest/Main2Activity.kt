package com.mest.mytest

import android.app.ProgressDialog
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.Toast
import com.mest.mytest.adapters.CompanyAdapter
import com.mest.mytest.models.Business
import com.mest.mytest.models.Company
import com.mest.mytest.service.BaseApiService
import com.mest.mytest.utils.UtilsApi
import com.vicpin.krealmextensions.queryAll
import com.vicpin.krealmextensions.save

import kotlinx.android.synthetic.main.activity_main2.*
import kotlinx.android.synthetic.main.content_main2.*

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Main2Activity : AppCompatActivity() {
    private var mApiService: BaseApiService? = null
    private var companyAdapter: CompanyAdapter? = null
    private var companyList: List<Company>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        setSupportActionBar(toolbar)
        mApiService = UtilsApi.apiService
        getPosts()


    }

    private fun populateList() {


        companyList = Company().queryAll()
        Toast.makeText(
            applicationContext, companyList!!.size.toString(), Toast.LENGTH_LONG
        )
            .show()
        if (!companyList!!.isEmpty()) {

            rv.visibility = View.VISIBLE

            companyAdapter = CompanyAdapter(this, (companyList as MutableList<Company>?)!!)
            rv.setHasFixedSize(true)
            rv.layoutManager = LinearLayoutManager(this)
            rv.adapter = companyAdapter
//        rv.itemAnimator = LandingAnimator()

        }
    }

    private fun getPosts() {

        mApiService!!.allBusiness.enqueue(object : Callback<Business> {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            override fun onResponse(
                call: Call<Business>
                , response: Response<Business>
            ) {
                when {
                    response.isSuccessful -> {
                        pbar.visibility = View.GONE
                        Log.d("Result", response.body().toString())
                        Toast.makeText(
                            this@Main2Activity
                            , "data fetch successful", Toast.LENGTH_LONG
                        ).show()
                        response.body()!!.save()

                        populateList()
                    }

                }
            }

            override fun onFailure(call: Call<Business>, t: Throwable) {
//                        if (mProgressDialog != null && mProgressDialog!!.isShowing) {
//                            mProgressDialog!!.dismiss()
//                        }
                pbar.visibility = View.GONE
                Log.e("debug", "onFailure: ERROR > " + t.message)


            }
        })

    }

}
