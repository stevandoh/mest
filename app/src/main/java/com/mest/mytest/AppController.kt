package com.mest.mytest

import android.app.Application
import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration


class AppController : Application() {

    override fun onCreate() {
        super.onCreate()
//        Fabric.with(this, Crashlytics())



        Realm.init(this)
        val config = RealmConfiguration.Builder()
            .name("Realmdb.realm")
//            .deleteAllFromRealm()
            .deleteRealmIfMigrationNeeded() // danger
            .build()
        Realm.setDefaultConfiguration(config)
        init()



    }


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
//        MultiDex.install(this)
    }


    private fun init() {
        instance = this
    }


    companion object {
        // The Realm file will be located in Context.getFilesDir() with name "default.realm"

        var instance: AppController? = null
            private set
        val TAG = AppController::class.java.simpleName

    }
}