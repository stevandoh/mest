package com.mest.mytest.adapters

import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.github.mikephil.charting.charts.BarChart

import com.mest.mytest.models.Business
import com.mest.mytest.models.Company
import com.vicpin.krealmextensions.queryFirst
import com.mest.mytest.models.Datum

import com.github.mikephil.charting.data.*

import android.graphics.Color
import com.mest.mytest.R


class CompanyAdapter(internal var context: Context, private var companyList: MutableList<Company>) :
    RecyclerView.Adapter<CompanyAdapter.CompanyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompanyAdapter.CompanyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.business_row, parent, false)
        return CompanyViewHolder(itemView)
    }

    init {
        setHasStableIds(true)
    }

    private fun getItem(position: Int): Company {
        return this.companyList[position]
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    override fun onBindViewHolder(holder: CompanyAdapter.CompanyViewHolder, position: Int) {
        val obj = getItem(position)
        val business = Business().queryFirst()
        val summary = business!!.analysis!!.phrases!!.summary
        val performance = business.analysis!!.phrases!!.performance
        val winner = business.analysis!!.phrases!!.winner
        val delta = business.analysis!!.phrases!!.delta
        val dataList: List<Datum>? = business.reach!!.data
        val brandReachTitle = business.reach!!.title
        val brandReachSubtitle = business.reach!!.subTitle

        holder.tvBrandAnalysis!!.text = business.analysis!!.title
        holder.tvBrandSubtitle!!.text = business.analysis!!.subTitle
        holder.tvBrandReachTitle!!.text = brandReachTitle
        holder.tvBrandReachSubtitle!!.text = brandReachSubtitle

//       list  Companies
        holder.tvCompanies!!.text = business.companies!!
            .map { it.name!! }
            .toString()
            .replace(",", " | ")
            .substringAfter("[")
            .substringBefore("]")

        //set chart
        val entries = ArrayList<BarEntry>()
        val colors = ArrayList<Int>()

        for (company in business.companies!!) {
            colors.add(Color.parseColor(company!!.color!!))
        }

        for (data in dataList!!) {
            entries.add(BarEntry(data.change.toFloat(), data.value.toFloat()))
        }
        val dataSet = BarDataSet(entries, business.reach!!.title)

        dataSet.colors = colors
        val barData = BarData(dataSet)
        barData.barWidth = 25f;
        holder.chart!!.data = barData
        holder.chart!!.animateXY(2000, 2000)

        val phrases = summary
            .plus("\n\n")
            .plus("-")
            .plus(performance)
            .plus("\n\n")
            .plus("-")
            .plus(winner)
            .plus("\n\n")
            .plus("-")
            .plus(delta)

        holder.tvBrandPhrases!!.text = phrases
//        holder.tvTitle!!.text = obj.name

    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int = if (true) companyList.size else 0


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    inner class CompanyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var data: Company? = null
        var tvTitle: TextView? = null
        var tvBrandAnalysis: TextView? = null
        var tvBrandSubtitle: TextView? = null
        var tvBrandPhrases: TextView? = null
        var tvBrandReachTitle: TextView? = null
        var tvBrandReachSubtitle: TextView? = null
        var chart: BarChart? = null
        var tvCompanies: TextView? = null


        init {
            tvTitle = itemView.findViewById<View>(R.id.textView) as TextView
            tvBrandAnalysis = itemView.findViewById<View>(R.id.tvBrandAnalysis) as TextView
            tvBrandSubtitle = itemView.findViewById<View>(R.id.tvBrandSubtitle) as TextView
            tvBrandPhrases = itemView.findViewById<View>(R.id.tvBrandPhrases) as TextView
            tvBrandReachTitle = itemView.findViewById<View>(R.id.tvBrandReachTitle) as TextView
            tvBrandReachSubtitle = itemView.findViewById<View>(R.id.tvBrandReachSubTitle) as TextView
            chart = itemView.findViewById<View>(R.id.chart) as BarChart
            tvCompanies = itemView.findViewById<View>(R.id.tvCompanies) as TextView

        }
    }
}