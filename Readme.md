# mytest

The app app list data fetched from an api and generate graphs

## Installation

clone the repo and build the app

Min sdk : 15

## Usage

To view stats and report

## Images

[image1](https://www.dropbox.com/s/15d0z848hf81hkq/mest_img1.png)
[image2](https://imgur.com/a/0sdymuQ)

#Apk link
[APK LINK](https://www.dropbox.com/s/wa34dy9nvrpd18s/mest.apk?dl=0)

## License

[MIT](https://choosealicense.com/licenses/mit/)
