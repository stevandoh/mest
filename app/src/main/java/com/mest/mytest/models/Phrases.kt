package com.mest.mytest.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class Phrases: RealmObject() {

    @PrimaryKey
    var id : String = UUID.randomUUID().toString()

    @SerializedName("summary")
    @Expose
    var summary: String? = null
    @SerializedName("performance")
    @Expose
    var performance: String? = null
    @SerializedName("winner")
    @Expose
    var winner: String? = null
    @SerializedName("delta")
    @Expose
    var delta: String? = null


}