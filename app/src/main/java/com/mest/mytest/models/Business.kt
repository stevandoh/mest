package com.mest.mytest.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class Business : RealmObject() {

    @PrimaryKey
    var id = "1";
    @SerializedName("companies")
    @Expose
    var companies: RealmList<Company>? = null
    @SerializedName("analysis")
    @Expose
    var analysis: Analysis? = null
    @SerializedName("reach")
    @Expose
    var reach: Reach? = null




}
