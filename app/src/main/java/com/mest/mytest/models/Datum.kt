package com.mest.mytest.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Datum : RealmObject(){

    @SerializedName("company_id")
    @Expose
    @PrimaryKey
    var companyId: String? = null
    @SerializedName("value")
    @Expose
    var value: Double = 0.toDouble()
    @SerializedName("change")
    @Expose
    var change: Double = 0.toDouble()




}