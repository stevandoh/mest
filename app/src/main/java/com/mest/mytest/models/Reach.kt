package com.mest.mytest.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Reach : RealmObject() {

    @SerializedName("id")
    @Expose
    @PrimaryKey
    var id: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("sub_title")
    @Expose
    var subTitle: String? = null
    @SerializedName("data")
    @Expose
    var data: RealmList<Datum>? = null



}