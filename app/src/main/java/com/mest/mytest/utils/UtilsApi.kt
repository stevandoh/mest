package com.mest.mytest.utils

import com.mest.mytest.service.BaseApiService


object UtilsApi {
    val BASE_URL_API = "https://api.myjson.com/"

    val apiService: BaseApiService
        get() = RetrofitClient.getClient(BASE_URL_API).create(BaseApiService::class.java)
}