package com.mest.mytest.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Analysis : RealmObject(){

    @SerializedName("id")
    @Expose
    @PrimaryKey
    var id: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("sub_title")
    @Expose
    var subTitle: String? = null
    @SerializedName("text")
    @Expose
    var text: String? = null
    @SerializedName("phrases")
    @Expose
    var phrases: Phrases? = null


}