package com.mest.mytest.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Company :RealmObject(){

    @SerializedName("id")
    @Expose
    @PrimaryKey
    var id: String? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("color")
    @Expose
    var color: String? = null



}